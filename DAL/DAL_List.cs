﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedClasses;
using System.IO;

namespace DAL
{
    public class DAL_List : IDAL
    {
        

        public void replacePass(User user, string newPass)
        {
            System.IO.File.SetAttributes(@"C:\Users\Noam\Documents\Visual Studio 2015\Projects\Milestones\SecurityText.txt", FileAttributes.Normal);
            
            List<User> lst = RetrieveAllUsers();
            int i = 0;
            while (lst.ElementAt(i).userName != user.userName && lst.ElementAt(i).password != user.password)
            {
                i++;
            }
            User newUser = new User(user.userName, newPass);
            lst.Remove(lst.ElementAt(i));       // remove the user who wants to change his password
            lst.Add(newUser);                   // add the user again with the new password to the end of the list

            string typeText = "";
            foreach (User tempUser in lst)
            {
                typeText += tempUser.userName + "\n" + tempUser.password + "\n";
            }
            //remove the exists file and create new file with the current data of users
            File.WriteAllText(@"C:\Users\Noam\Documents\Visual Studio 2015\Projects\Milestones\SecurityText.txt", typeText);
            
        }

        public List<User> RetrieveAllUsers()
        {

            FileStream fs = new FileStream(@"C:\Users\Noam\Documents\Visual Studio 2015\Projects\Milestones\SecurityText.txt", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            string data = sr.ReadLine();
            List<User> lst = new List<User>();

            while (data != null)
            {
                string userName = data;
                data = sr.ReadLine();
                string password = data;                     // assume password exists.
                lst.Add(new User(userName, password));      // add the user from the file to the list.
                data = sr.ReadLine();

            }
            sr.Close();
            fs.Close();
            return lst;                                     // return the list with all the users.

        }
    }
}
