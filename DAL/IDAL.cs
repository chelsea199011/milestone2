﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedClasses;

namespace DAL
{
    public interface IDAL
    {
        List<User> RetrieveAllUsers();   //return a list of users that signed up from the text file.
        void replacePass(User user, String newPass);  // replace the pass in the text file.
        List</*path of file*/> listOfFiles(/*path of folder*/);
        String RetriveFileText(/*path of file*/);   //also used for log files.
        void insertLineToLogFile(String line);
        

    }
}
