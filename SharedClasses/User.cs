﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedClasses
{
    public class User          // User class with fields of username and password
    {
        private String _userName;
        public String userName
        {
            get
            {
                return _userName;
            }
        }
        private String _password;
        public String password
        {
            get
            {
                return _password;
            }

        }
        private int _premission; // 1 to regular, 2 to manager, 3 to admin.
        public int premission
        {
            get
            {
                return _premission;
            }
        }

        public User(String userName, String password)
        {
            this._userName = userName;
            this._password = password;
        }

        public void setpassword(String password)
        {
            this._password = password;
        }
    }
}
