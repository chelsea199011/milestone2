﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedClasses;

namespace BL
{
    public interface IBL
    {
        User searchByUserName(String userName, String pass);                    // search user and return him
        bool findUser(String userName, String pass);                            // search user and return true if he exists           
        bool ChangePassword(String userName, String newPass, String oldPass);   // change passowrd to an exsists user and return true if successed
        bool validPass(String newPass);                                         // return true if the password valid
        string randomPass();                                                    // create and return random valid password
        //הצפנה
        double sumPoints(String text);
        void addUser(User user);
        void deleteUser(User user);
        bool checkPremission(User user, int premission);
        void changePremission(User user, int premission);
        void updateLog(String str);
        Dictionary</*path of File*/, Double> listFileWithPoints(/* path of folder */);

    }
}
