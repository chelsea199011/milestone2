﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using SharedClasses;
using System.IO;

namespace BL
{
    public class BL_v1 : IBL
    {
        private const int pass_len = 8;
        private IDAL _itsDAL;
        public BL_v1()
        {
            _itsDAL = new DAL_List();
        }

        public bool findUser(string userName, string pass)
        {
            if (searchByUserName(userName, pass) == null)
                return false;
            return true;
        }
        public bool validPass(string newpass)
        {
            if (newpass.Length != 8)
                return false;
            else
                for (int i = 0; i < newpass.Length; i++)
                {
                    if (newpass[i] > 48 && newpass[i] < 57)
                    {
                        return true;
                    }
                    if ((newpass[i] < 48 || newpass[i] > 57) && i == newpass.Length - 1)
                    {
                        return false;
                    }
                }

            return false;
        }
        public User searchByUserName(String userName, String pass) 
        {
            List<User> lst = _itsDAL.RetrieveAllUsers();

            User temp;
            int i = lst.Count - 1;
            while (i >= 0)
            {
                temp = lst.ElementAt(i);
                if (temp.userName == userName && temp.password == pass)
                    return temp;
                i--;

            }

            return null;
        }

        public bool ChangePassword(string userName, string newPass, string oldPass) 
        {
            //if the password is not proper return false
            if (validPass(newPass) == false)
            {
                return false;
            }

            else
            {
                User user1 = searchByUserName(userName, oldPass);
                if (user1 == null) return false;
                else if (!oldPass.Equals(user1.password))         //check if the oldpass is realy the oldpass
                    return false;
                else
                {
                    user1.setpassword(newPass);      //change the password
                    _itsDAL.replacePass(user1, newPass);     //return true if was a replace
                    return true;
                }
            }
        }


        public string randomPass()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            string new_password = "";
            Random rnd = new Random();
            for (int i = 0; i < pass_len; i++)          // create password in length 8
            {
                int index = rnd.Next(0, chars.Length);
                new_password += chars[index];
            }

            if (!hasNumber(new_password))               
            {
                int index = new Random().Next(0, new_password.Length);
                int number = new Random().Next(0, 10);
                new_password = replaceChar(new_password, number.ToString()[0], index); // change the last character in the password to digit if
            }
            Console.WriteLine(new_password);
            return new_password;
        }

        private Boolean hasNumber(string s)         //checks if exists a number in the password
        {
            return s.Any(char.IsDigit);
        }

        private string replaceChar(string str, char c, int index)   // change char in index i in the string
        {
            try
            {
                StringBuilder sb = new StringBuilder(str);
                sb[index] = c;
                return sb.ToString();
            }
            catch
            {
                throw new Exception("Error was occured while generating new password");
            }
        }

        public double sumPoints(string text)
        {
            double score = 0.0;
            string[] rows = text.Split(System.Environment.NewLine.ToCharArray());
            for (int i = 0; i < rows.Length; i++)
            {
                string[] words = rows[i].Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                for (int j = 0; j < words.Length; j++)
                    score += wordScore(words[j]);

            }
            return score;
        }

        private double wordScore(string word)
        {
            Dictionary<string, double> scores = scoresTable();
            foreach (var item in scores)
            {
                if (item.Key.ToLower() == word.ToLower())
                    return item.Value;
            }

            return 0.0;
        }

        private Dictionary<string, double> scoresTable()
        {
            Dictionary<string, double> scores = new Dictionary<string, double>();
            scores.Add("classified", 1.0);
            scores.Add("secret", 1.0);
            scores.Add("password", 1.0);
            scores.Add("hash", 0.75);
            scores.Add("break-in", 0.6);
            scores.Add("sensitive", 0.8);
            scores.Add("restricted", 1);
            scores.Add("encrypt", 0.8);
            scores.Add("private", 0.9);
            scores.Add("credential", 0.6);
            scores.Add("pin", 0.8);
            scores.Add("key", 0.8);
            scores.Add("admin", 0.6);
            scores.Add("virus", 0.75);
            scores.Add("worm", 0.75);

            return scores;
        }

        public Dictionary<string, double> filesScore(string path)
        {
            Dictionary<string, double> res = new Dictionary<string, double>();
            DirectoryInfo dinfo = new DirectoryInfo(@path);
            FileInfo[] Files = dinfo.GetFiles("*.txt");

            foreach (FileInfo file in Files)
            {
                double score = sumPoints(_itsDAL.RetriveFileText(file.FullName));
                res.Add(file.Name, score);
            }

            return res;

        }
    }
}
