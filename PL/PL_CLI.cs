﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL; 


namespace PL
{
    public class PL_CLI : IPL
    {
        private IBL _itsBL;
        public PL_CLI()
        {
            _itsBL = new BL_v1();
        }
        public void Run()
        {
            string userTemp = "";
            string passTemp = "";
            bool isFind = false;
            int countAttemps = 0;
            while (!isFind && countAttemps < 5) // trying to log in until 5 attempts.
            {
                Console.WriteLine("Hello user");
                Console.WriteLine("Please enter your user name: ");
                userTemp = Console.ReadLine();
                Console.WriteLine("Please enter your password: ");
                passTemp = Console.ReadLine();
                isFind = this._itsBL.findUser(userTemp, passTemp);
                countAttemps++;
                if (!isFind)
                {
                    Console.WriteLine("Invalid username or password");
                    Console.WriteLine("You have " + (5 - countAttemps) + " attemps");
                }

            }
            if (!isFind)              //login failed
                Console.WriteLine("No more attemps to login. Goodbye");
            else                     //login success
            {
                Console.WriteLine("type 1 if you want to change the password,type 2 if you want to change the password randomly");
                string x = Console.ReadLine();
                if (x == "1")         //change password individual
                {
                    Console.WriteLine("type the new password");

                    String newPass = Console.ReadLine();
                    bool changed = _itsBL.ChangePassword(userTemp, newPass, passTemp);
                    while (!changed)
                    {
                        Console.WriteLine("incorrect password,type new password");
                        newPass = Console.ReadLine();
                        changed = this._itsBL.ChangePassword(userTemp, newPass, passTemp);
                    }
                }
                else if (x == "2")         //change password with random pass.
                {
                    String newPass = _itsBL.randomPass();
                    bool changed = this._itsBL.ChangePassword(userTemp, newPass, passTemp);
                }
                Console.WriteLine("Password has been succesfully changed, click enter to exit");
                Console.ReadLine();
            }



        }
    }
}
