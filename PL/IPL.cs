﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedClasses;

namespace PL
{
     interface IPL
    {
        void Run(); // runs the project.
        void printTextFiles(String text);
    }
}
